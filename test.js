/// <reference types="cypress" />


describe('open the page', () => {
    beforeEach(() => {
      //Open the page
      cy.visit("https://computer-database.gatling.io/computers")
    })

    it('1.Click to edit Commodore 64 i. Create a negative test case to ensure a failure validation displays', () => {
        cy.get('#searchbox').type('inexistent test article{enter}')
        cy.get('em').should("not.exist");;
        cy.get('a').contains('Commodore 64').click() 
        cy.get('#introduced').clear().type('1983-09-02')
        cy.get('#discontinued').clear().type('1995-09-02')
        cy.get('#company').select('Apple Inc.')
        cy.get('.primary').click()
        cy.get('.alert-message').invoke('text').should('include', 'has been updated')
      })
     
      it('1.Click to edit Commodore 64 ii.Create a positive test case to ensure that valid data is updated successfully', () => {
          cy.get('#searchbox').type('Commodore 64{enter}')
          cy.get('em').should("not.exist");;
          cy.get('a').contains('Commodore 64').click() 
          cy.get('#introduced').clear().type('1983-09-02')
          cy.get('#discontinued').clear().type('1995-09-02')
          cy.get('#company').select('Apple Inc.')
          cy.get('.primary').click()
          cy.get('.alert-message').invoke('text').should('include', 'has been updated')
        })
  
        it('2. Filter computer list by “HP” and create a map of the returned data', () => {
            cy.get('#searchbox').type('HP{enter}');
            cy.get('table').each(($e1, index, $list)=>{ //iterating through array of elements
             const StoreText = $e1.text() 
            cy.log(StoreText)
        })
    })
          
        it('3. Filter computer list by “IBM” and return a list of computer names on the LAST page of the results a.Print the list of computer names', () => {
            var i=1
            cy.get('#searchbox').type("IBM")
            cy.get('#searchsubmit').click()
            cy.get('.next > a').click()
            cy.get('.next > a').click()
            for(i==1;i<6;i++){
               
                cy.get('tbody > :nth-child('+i+') > :nth-child(1)').invoke('text').then((list1) => {
                    cy.log(list1)
                })
            } 
        })
        

})

